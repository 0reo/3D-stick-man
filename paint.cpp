#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
//#include "prims.h"
#include <cstdlib>

/* display function - GLUT display callback function
 *		clears the screen, draws a square, and displays it
 */

// prims * test = new prims();
float i = 1.0;
float point1[2] = {0.0f,0.0f};
float point2[2]={0.0,1.0};
float point3[2]={1.0,1.0};
float point4[2]={1.0,0.0};
float eyes[3]={0.0, 0.0, 30.0};
float legRotate = 45.0;
float armRotate = 90.0;
float pos = 1;
float body, head, head2, leftArm, leftArm2, rightArm, rightArm2, leftLeg, leftLeg2, rightLeg, rightLeg2 = 0.0;
bool bBody, bHead, bHead2, bLeftArm, bLeftArm2, bRightArm, bRightArm2, bLeftLeg, bLeftLeg2, bRightLeg, bRightLeg2, forward, back= false;
bool animate = false;
int shape = 99;
int count = 0;

float color1[3]={1.0,0.0,0.0};
int LMB, RMB = 0;
int n = 200;
GLUquadric *quadric = gluNewQuadric();




void init(void)
{


    glViewport(0, 0, 300, 300);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    //glOrtho(-300,300,-300,300,-300,300);
    gluPerspective(45.0f, 1.0f, 1.0f, 100.0f);


    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_DEPTH_TEST);

    glClearColor(0,0,0,0);
}

void display(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction


    glColor3f(1.0, 0.0, 0.0);

    glPushMatrix();                 //body
    glRotatef(body, 0.0, 1.0, 0.0);
    glRotatef(-90.0, 1, 0, 0);
    glTranslatef(0.0, 0.0, 0.0);
    gluCylinder(quadric, 2.0f, 2.0f, 6.0f, 40, 40);

    glPushMatrix();             //head
    glTranslatef(0.0,0.0,8.5);
    glRotatef(head, 0.0, 0.0, 1.0);
    glRotatef(head2, 1.0, 0.0, 0.0);
    gluSphere(quadric, 1.5, 40,40);
    glPushMatrix();
    glColor3f(1.0,1.0,1.0);
    glTranslatef(0.0,-1.5,-0.5);
    gluSphere(quadric, 0.25, 40,40);
    glPopMatrix();
    glPopMatrix();

    glPushMatrix();             //right arm
    glColor3f(0.0, 1.0, 1.0);
    glRotatef(180.0, 1.0, 0.0, 0.0);
    glTranslatef(3.0, 0.0, -6.0);
    glRotatef(rightArm, 1.0, 0.0, 0.0);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);

    glPushMatrix();         //right arm 2
    glColor3f(0.0,1.0,1.0);
    glTranslatef(0.0, 0.0, 4.0);
    glRotatef(rightArm2,1.0,0.0,0.0);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);
    glPopMatrix();
    glPopMatrix();

    glPushMatrix();             //left arm
    glColor3f(1.0, 1.0, 0.0);
    glRotatef(180.0, 1.0, 0.0, 0.0);
    glTranslatef(-3.0, 0.0, -6.0);
    glRotatef(leftArm, 1.0, 0.0, 0.0);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);

    glPushMatrix();
    glColor3f(0.0, 1.0, 0.0);
    glTranslatef(0.0, 0.0, 4.0);
    glRotatef(leftArm2,1.0,0.0,0.0);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);
    glPopMatrix();
    glPopMatrix();

    glPushMatrix();//right leg

    glColor3f(1.0, 0.0, 0.0);
    glRotatef(rightLeg, 1.0, 0.0, 0.0);
    glTranslatef(1.5, 0.0, -4.5);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);

    glPushMatrix();
    glColor3f(0.0,1.0,0.0);
    glRotatef(rightLeg2, 1.0, 0.0, 0.0);
    glTranslatef(0.0, 0.0, -4.0);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);
    glPopMatrix();
    glPopMatrix();

    glPushMatrix();//left leg
    glColor3f(1.0, 0.0, 0.0);
    glRotatef(leftLeg, 1.0, 0.0, 0.0);
    glTranslatef(-1.5, 0.0, -4.5);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);
    glPushMatrix();
    glColor3f(0.0, 1.0, 0.0);
    glRotatef(leftLeg2, 1.0, 0.0, 0.0);
    glTranslatef(0.0, 0.0, -4.0);
    gluCylinder(quadric, 0.5f, 0.5f, 4.0f, 40, 40);
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    //glutPostRedisplay();
    //glFlush();
}


void spcFunc(int key, int x, int y)
{

    switch (key)
    {
    case GLUT_KEY_UP:
        eyes[0]+=1.0f;
        break;

    case GLUT_KEY_DOWN:
        eyes[0]-=1.0f;
        break;

    case GLUT_KEY_LEFT:
        eyes[2]-=1.0f;
        break;

    case GLUT_KEY_RIGHT:
        eyes[2]+=1.0f;
        break;
    }
    glutPostRedisplay();
}



void mouse(int btn, int state, int x,int y)
{
    if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        float pos = 1.0;
        LMB = true;
    }
    else
    {
        LMB = false;
    }
    if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
    {
        float pos = -1.0;
        RMB = true;
    }
    else
    {
        RMB = false;
    }
}

void keyboard(unsigned char key, int x,int y)
{
    if (key == 'a')
    {
        forward = true;
        back=false;
    }
    else if (key == 's')
    {
        forward = false;
        back=true;
    }
}



void handel_menu (int choice)
{
    shape = choice;

}

void menu()
{
    int menu, colourMenu, bodyPartMenu;
    menu = glutCreateMenu(handel_menu);


    glutSetMenu(bodyPartMenu);
    glutAddMenuEntry("body", 0);
    glutAddMenuEntry("head x", 1);
    glutAddMenuEntry("head y", 2);
    glutAddMenuEntry("right arm x", 3);
    glutAddMenuEntry("right arm 2 x", 4);
    glutAddMenuEntry("left arm x", 5);
    glutAddMenuEntry("left arm 2 x", 6);
    glutAddMenuEntry("right leg x", 7);
    glutAddMenuEntry("right leg 2 x", 8);
    glutAddMenuEntry("left leg x", 9);
    glutAddMenuEntry("left leg 2 x", 10);
    glutAddMenuEntry("Start Walking", 11);

    glutAttachMenu(GLUT_RIGHT_BUTTON);

}

void idle()
{
    glutSwapBuffers();
    if (head > 90.0)
        head--;
    if (head < -90.0)
        head++;

    if (head2 > 45)
        head2--;
    if (head2 < -60)
        head2++;

    if (rightArm > 90.0)
        rightArm--;
    if (rightArm < -180.0)
        rightArm++;

    if (rightArm2 > 0.0)
        rightArm2++;
    if (rightArm2 < -160.0)
        rightArm2++;

    if (leftArm > 90.0)
        leftArm--;
    if (leftArm < -180.0)
        leftArm++;

    if (leftArm2 > 0.0)
        leftArm2++;
    if (leftArm2 < -160.0)
        leftArm2++;

    if (rightLeg > 90.0)
        rightLeg--;
    if (rightLeg < -90.0)
        rightLeg++;

    if (rightLeg2 < 0.0)
        rightLeg2++;
    if (rightLeg2 > 140.0)
        rightLeg2--;

    if (leftLeg2 < 0.0)
        leftLeg2++;
    if (leftLeg2 > 140.0)
        leftLeg2--;

    if (LMB || RMB)
    {
        if (shape == 0)
        {
            if (forward)
                pos*body--;
            if (back)
                pos*body++;
            glFlush();
        }
        else if (shape == 1)
        {
            if (forward)
                pos*head--;
            if (back)
                pos*head++;
            glFlush();
        }
        else if (shape == 2)
        {
            if (forward)
                pos*head2++;
            if (back)
                pos*head2--;
            glFlush();
        }
        else if (shape == 3)
        {
            if (forward)
                pos*rightArm--;
            if (back)
                pos*rightArm++;
            glFlush();
        }
        else if (shape == 4)
        {
            if (forward)
                pos*rightArm2--;
            if (back)
                pos*rightArm2++;
            glFlush();
        }
        else if (shape == 5)
        {
            if (forward)
                pos*leftArm--;
            if (back)
                pos*leftArm++;
            glFlush();
        }
        else if (shape == 6)
        {
            if (forward)
                pos*leftArm2--;
            if (back)
                pos*leftArm2++;
            glFlush();
        }
        else if (shape == 7)
        {
            if (forward)
                pos*rightLeg--;
            if (back)
                pos*rightLeg++;
            glFlush();
        }
        else if (shape == 8)
        {
            if (forward)
                pos*rightLeg2++;
            if (back)
                pos*rightLeg2--;
            glFlush();
        }
        else if (shape == 9)
        {
            if (forward)
                pos*leftLeg++;
            if (back)
                pos*leftLeg--;
            glFlush();
        }
        else if (shape == 10)
        {
            if (forward)
                pos*leftLeg2++;
            if (back)
                pos*leftLeg2;
            glFlush();
        }
        else if (shape == 11)
        {
            animate = !animate;
        }
    }


    if (animate)
    {
        if (leftArm > 60.0)
            pos = -1.0;
        if (leftArm < -60.0)
            pos = 1.0;
        leftArm+=pos;
        rightArm-=pos;
        leftLeg-=pos;
        rightLeg+=pos;
    }
    glutPostRedisplay();
}

/* main function - program entry point */
int main(int argc, char** argv)
{
    glutInit(&argc, argv);		//starts up GLUT
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Stick Man");	//creates the window
    glutDisplayFunc(display);	//registers "display" as the display callback function
    glutIdleFunc(idle);
    glutSpecialFunc(spcFunc);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    init();
    menu();
    glutMainLoop();				//starts the event loop

    return(0);					//return may not be necessary on all compilers
}

